Waterdrop
=========

About Waterdrop
----------------
Uliweb is a Python based web framework created by limodou <limodou@gmail.com>, 
and Waterdrop is an apps collection project for uliweb framework to provide 
a simple workflow engine.

License
------------
Waterdrop is released under BSD license.

