#coding=utf-8
from uliweb import settings
from email.mime.text import MIMEText
from gevent.queue import Queue

import smtplib
import gevent


#邮件操作类
class MailImpl(object):

    def __init__(self, to_list, subject, content, **kwargs):
        self.__tolist = to_list
        self.__sub = subject
        self.__content = content

    #发送邮件
    def send_mail(self):
        '''
        to_list:发给谁
        subject:主题
        content:内容
        send_mail("aaa@126.com","sub","content")
        '''
        #设置服务器，用户名、口令以及邮箱的后缀
        mail_host = settings.MAILCONTROL.HOST
        mail_user = settings.MAILCONTROL.USER
        mail_display = settings.MAILCONTROL.DISPLAY
        mail_pass = settings.MAILCONTROL.PASSWORD
        mail_postfix = settings.MAILCONTROL.POSTFIX
        #邮件内容
        me = mail_display+"<"+mail_user+"@"+mail_postfix+">"
        msg = MIMEText(self.__content)
        msg['Subject'] = self.__sub
        msg['From'] = me
        msg['To'] = ";".join(self.__tolist)
        try:
            s = smtplib.SMTP()
            s.connect(mail_host)
            s.login(mail_user,mail_pass)
            s.sendmail(me, self.__tolist, msg.as_string())
            return {'result' : True}
        except Exception, e:
            return {'result' : False, 'logs' : repr(e)}
        finally:
            s.close()


#邮件管理类
class MailManager(object):

    #静态队列：邮件发送列表
    mail_queue = Queue()
    listen_mail_on = False
    listen_mail_gfunc = None

    #监控邮件列表，发送邮件
    @classmethod
    def listen_mail_queue(cls):
        while True:
            #获得新增发送邮件信息
            maildetail = cls.mail_queue.get()
            if not maildetail or len(maildetail.keys()) <= 0:
                continue
            #初始化
            mi = MailImpl(**maildetail)
            gevent.spawn(mi.send_mail)

    #邮件内容进入队列
    @classmethod
    def push_mail_queue(cls, to_list, subject, content, **kwargs):
        if not settings.MAILCONTROL.MAIL_LISTEN_ON:
            return
        if not cls.listen_mail_on:
            listen_mail_gfunc = gevent.spawn(cls.listen_mail_queue)
            cls.listen_mail_on = True
        if 'direct_send' in kwargs and kwargs['direct_send']:
            mi = MailImpl(to_list = to_list, subject = subject, content = content)
            mi.send_mail()
        else:
            cls.mail_queue.put(dict(kwargs, to_list = to_list, subject = subject, content = content))