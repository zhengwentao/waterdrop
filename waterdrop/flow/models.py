#coding=utf-8
from uliweb.orm import Field, Model, Reference, datetime, BLOB, PICKLE


#流程实例表，记录流程实例化后状态
class WFWaterdrop(Model):
    created_date = Field(datetime.datetime, verbose_name='创建时间', auto_now_add=True)
    modified_date = Field(datetime.datetime, verbose_name='修改时间', auto_now=True, auto_now_add=True)
    define = Reference('wfdefine', collection_name='waterdrop')
    end = Field(bool, verbose_name='是否已结束', default = False, required=True, index=True)
    status = Reference('wfstatus', collection_name='waterdrop')

#步骤日志表，记录流程步骤日志情况
class WFFlow(Model):
    created_date = Field(datetime.datetime, verbose_name='创建时间', auto_now_add=True)
    modified_date = Field(datetime.datetime, verbose_name='修改时间', auto_now=True, auto_now_add=True)
    waterdrop = Reference('wfwaterdrop', collection_name='flow')
    status = Field(int, verbose_name='步骤执行状态', default = 0, required=True, index=True)
    result = Field(bool, verbose_name='步骤执行结果', default = False, required=True, index=True)
    step = Field(str, max_length = 500, verbose_name='执行函数', default = '', required=True)
    condition = Field(str, max_length = 500, verbose_name='条件内容', default = '', required=True)
    paramsnap = Field(str, max_length = 500, verbose_name='属性快照', default = '', required=True)
    logs = Field(BLOB, verbose_name='日志', default = '')

class WFArgument(Model):
    created_date = Field(datetime.datetime, verbose_name='创建时间', auto_now_add=True)
    modified_date = Field(datetime.datetime, verbose_name='修改时间', auto_now=True, auto_now_add=True)
    waterdrop = Reference('wfwaterdrop', collection_name='argument')
    param = Reference('wfparam', collection_name='argument')
    value = Field(PICKLE)

#状态日志表，记录流程状态日志情况
class WFStatusFlow(Model):
    created_date = Field(datetime.datetime, verbose_name='创建时间', auto_now_add=True)
    modified_date = Field(datetime.datetime, verbose_name='修改时间', auto_now=True, auto_now_add=True)
    waterdrop = Reference('wfwaterdrop', collection_name='statusflow')
    oldstatus = Reference('wfstatus', collection_name='statusflow_old')
    newstatus = Reference('wfstatus', collection_name='statusflow_new')
    paramsnap = Field(str, max_length = 500, verbose_name='属性快照', default = '')