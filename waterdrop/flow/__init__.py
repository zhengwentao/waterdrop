#coding=utf-8
import logging

#定义handler的输出格式  
formatter = logging.Formatter('%(asctime)s - %(name)s - %(filename)s - %(module)s - %(levelname)s - %(message)s')

#记录流程步骤操作日志
FlowLog = logging.getLogger('flow')
FlowLog.setLevel(logging.DEBUG)
#用于写入日志文件
fh = logging.FileHandler('flowlog.log')
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
# 给logger添加handler
FlowLog.addHandler(fh)