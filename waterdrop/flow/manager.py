#coding=utf-8
from uliweb.orm import get_model
from gevent.queue import Queue

from inst import Waterdrop
from statusrun import Waterdrop_statusrun

import gevent
import gipc



#流程管理类
class Waterdrop_manager():
    #创建流程, 返回流程实例ID
    @classmethod
    def create(cls, defineID, status_code, **kwargs):
        #新建主流程实例记录
        WFWaterdrop = get_model('wfwaterdrop')
        WFStatus = get_model('wfstatus')
        wfs = WFStatus.get(WFStatus.c.code == status_code)
        if not wfs:
            return None
        wfw = WFWaterdrop(define = defineID, status = wfs)
        wfw.save()
        #将参数写入实例参数表
        WFStatus = get_model('wfstatus')
        WFParam = get_model('wfparam')
        WFArgument = get_model('wfargument')
        for each_param in list(WFParam.all()):
            wfa = WFArgument(waterdrop = wfw, param = each_param)
            if kwargs != None and each_param.name in kwargs:
                wfa.value = kwargs[each_param.name]
            else:
                wfa.value = each_param.default_value
            wfa.save()
        return wfw.id

    #静态队列：属性变化通知
    param_queue = Queue()
    #静态队列：流程工作队列
    list_queue = {}
    #静态队列：相同流程等待队列
    waiting_queue = []
    #监听是否已启动
    listen_param_on = False
    listen_param_gfunc = None

    #启动流程监控属性列表
    @classmethod
    def listen_param(cls):
        while True:
            #清理结束流程
            for x in cls.list_queue.keys():
                gpro = cls.list_queue[x]['gpro']
                if  not gpro.is_alive():
                    del cls.list_queue[x]
                    for i, ewfid in enumerate(cls.waiting_queue):
                        if x == 'w_'+str(ewfid):
                            cls.param_queue.put(ewfid)
                            del cls.waiting_queue[i]
                            break
            #获得属性变化信息
            wfid = cls.param_queue.get()
            if 'w_'+str(wfid) not in cls.list_queue:
                #新流程进入
                wd = Waterdrop(wfid)
                cls.list_queue['w_'+str(wfid)] = { 'gpro' : gipc.start_process(target = wd.go, daemon = True) }
            else:
                cls.waiting_queue.append(wfid)
            gevent.sleep(0)

    #更新属性
    @classmethod
    def modify_param(cls, workflowid, need_step = True, direct_step = False, **kwargs):
        if not cls.listen_param_on:
            cls.listen_param_gfunc = gevent.spawn(cls.listen_param)
            cls.listen_param_on = True
        if kwargs == None or len(kwargs.keys()) == 0:
            return False
        WFWaterdrop = get_model('wfwaterdrop')
        wfw = WFWaterdrop.get(WFWaterdrop.c.id == workflowid)
        if not wfw:
            return False
        get_model('wfargument')
        get_model('wfparam')
        for each_arg in list(wfw.argument):
            if each_arg.param.name in kwargs:
                each_arg.value = kwargs[each_arg.param.name]
                each_arg.save()
        #提示已有流程属性更新
        if need_step:
            if direct_step:
                wd = Waterdrop(workflowid)
                wd.go()
            else:
                cls.param_queue.put(workflowid)
        return True

    #工作流状态前进
    @classmethod
    def forward_workflow(cls, workflowid, **kwargs):
        wd = Waterdrop(workflowid)
        pdict = wd.get_argument()
        ws = Waterdrop_statusrun(waterdropid = workflowid, currentstatus_code = pdict['currstatus'], paramdict = kwargs)
        return ws.forward()

    #工作流状态后退
    @classmethod
    def fallback_workflow(cls, workflowid, **kwargs):
        wd = Waterdrop(workflowid)
        pdict = wd.get_argument()
        ws = Waterdrop_statusrun(waterdropid = workflowid, currentstatus_code = pdict['currstatus'], paramdict = kwargs)
        return ws.fallback()