#coding=utf-8


#判断条件是否为真
class Waterdrop_judge():

    #条件内容
    __condition = None
    #属性
    __param_dict = None

    #输入属性dict
    def get_param(self):
        return self.__param_dict
    def set_param(self, paramdict = None):
        self.__param_dict = paramdict

    #初始化
    def __init__(self, condition, paramdict = None):
        self.__condition = " "+condition+" "
        self.__param_dict = paramdict

    #判断
    def judge(self):
        #为空时自动成立
        if not self.__condition:
            return True
        #执行代码
        try:
            import copy
            copy_param_dict = copy.deepcopy(self.get_param())
            exec "res_for_workflow_judge = "+self.__condition in copy_param_dict
            return copy_param_dict['res_for_workflow_judge']
        except:
            return False
