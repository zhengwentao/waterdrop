#coding=utf-8
from uliweb.orm import get_model

from waterdrop.define.query import Waterdrop_query
from condjudge import Waterdrop_judge
from stepexec import Waterdrop_execute

import gevent



#流程类
class Waterdrop():

    #主要信息：ID
    __waterdropid = None
    #流程定义ID
    __defineid = None
    #是否已终止
    __end = False
    #go运行时是否已检查过mix
    __checked_mix = False

    @property
    def workflowid(self):
        return self.__waterdropid
    @property
    def end(self):
        return self.__end

    #正在执行的step
    __stepdict = {}

    @property
    def empty(self):
        self.clear_stepexec()
        return self.__checked_mix and len(self.__stepdict.keys()) == 0

    #初始化
    def __init__(self, waterdropid):
        WFWaterdrop = get_model('wfwaterdrop')
        wfw = WFWaterdrop.get(waterdropid)
        if not wfw:
            raise TypeError, 'WFWaterdrop dose not exists.'
        self.__waterdropid = wfw.id
        self.__defineid = wfw.define.id
        self.__end = wfw.end

    #执行步骤
    def go(self):
        #清理已完成的步骤
        self.clear_stepexec()
        if self.__end:
            raise TypeError, 'WorkFlow has been ended.'
        #获得条件与步骤组合
        mixlist = Waterdrop_query.query_mix_from_define(self.__defineid)
        self.__checked_mix = True
        if not mixlist or len(mixlist) <= 0:
            raise TypeError, 'There is not step in WorkFlow, please check your settings.'
        #获得属性参数
        paramdict = self.get_argument()
        #执行函数组合
        for estep, econd, is_last in mixlist:
            if estep['name'] in self.__stepdict:
                continue
            cond_judge = Waterdrop_judge(econd['condition'], paramdict)
            if cond_judge.judge():
                step_exec = Waterdrop_execute(waterdropid = self.__waterdropid,
                                              step = estep['function'],
                                              condition = econd['condition'],
                                              is_last = is_last,
                                              paramdict = paramdict)
                self.__stepdict[estep['name']] = gevent.spawn(step_exec.execute)
        if len(self.__stepdict.values()) > 0:
            gevent.joinall(self.__stepdict.values())
            from manager import Waterdrop_manager
            Waterdrop_manager.param_queue.put(self.__waterdropid)

    #清理已经执行完毕的步骤
    def clear_stepexec(self):
        for gk in self.__stepdict.keys():
            if self.__stepdict[gk].successful():
                del self.__stepdict[gk]

    def get_argument(self):
        args = {}
        WFArgument = get_model('wfargument')
        get_model('wfparam')
        for each_arg in list(WFArgument.filter(WFArgument.c.waterdrop == self.__waterdropid)):
            args[each_arg.param.name] = each_arg.value
        return args