#coding=utf-8
from uliweb import settings
from uliweb.orm import get_model

from condjudge import Waterdrop_judge

import json



#状态变更执行
class Waterdrop_statusrun():
    '''
    初始化实例后，必须赋值__info_dict，提供流程实例ID等必要信息。
    属性__param_dict可选，作为执行时的参数输入。
    '''
    #主要信息
    #日志ID
    __statusflowid = None
    #流程实例ID
    __waterdropid = None
    #属性
    __param_dict = None

    #输入属性dict
    def get_param(self):
        return self.__param_dict
    def set_param(self, paramdict = None):
        self.__param_dict = paramdict

    #初始化
    def __init__(self, waterdropid, currentstatus_code, paramdict = None, statusflowid = None):
        self.__waterdropid = waterdropid
        self.__currentstatus_code = currentstatus_code
        self.__statusflowid = statusflowid
        self.__param_dict = paramdict

    #检查数据库中的flow信息
    def query_flow(self):
        WFStatusFlow = get_model('wfstatusflow')
        #得到记录的流程日志
        if self.__statusflowid:
            flow = WFStatusFlow.get(WFStatusFlow.c.id == self.__statusflowid)
        else:
            flow = WFStatusFlow(waterdrop = self.__waterdropid, paramsnap = json.dumps(self.__param_dict) if self.__param_dict else '')
            flow.save()
            self.__statusflowid = flow.id
        return flow

    #写入步骤执行日志状态
    def save_log_status(self, oldstatusid, newstatusid):
        flow = self.query_flow()
        #记录状态
        flow.oldstatus = oldstatusid
        flow.newstatus = newstatusid
        flow.save()

    #执行步骤前进到下一个节点
    def forward(self):
        WFWaterdrop = get_model('wfwaterdrop')
        wfw = WFWaterdrop.get(WFWaterdrop.c.id == self.__waterdropid)
        if not wfw:
            raise TypeError, 'WFWaterdrop dose not exists.'
        elif wfw.status.code != self.__currentstatus_code:
            raise ValueError, 'WFWaterdrop had passed current status.'
        from manager import Waterdrop_manager
        Waterdrop_manager.modify_param(self.__waterdropid, need_step = False, **self.__param_dict)
        WFArgument = get_model('wfargument')
        wfargs = {}
        for earg in list(WFArgument.filter(WFArgument.c.waterdrop == self.__waterdropid)):
            wfargs[earg.param.name] = earg.value
        WFStatusRoute = get_model('wfstatusroute')
        for nextstatus in list(WFStatusRoute.filter(WFStatusRoute.c.from_status == wfw.status.id).order_by(WFStatusRoute.c.weight.desc())):
            if not nextstatus.condition or not nextstatus.condition.condition or Waterdrop_judge(nextstatus.condition.condition, wfargs).judge():
                self.save_log_status(wfw.status.id, nextstatus.to_status.id)
                wfw.status = nextstatus.to_status
                wfw.save()
                Waterdrop_manager.modify_param(self.__waterdropid, currstatus = nextstatus.to_status.code)
                if nextstatus.to_status.autonext:
                    self.__currentstatus_code = nextstatus.to_status.code
                    self.__statusflowid = None
                    return self.forward()
                else:
                    return nextstatus.to_status.code
        return settings.WORKFLOWINIT.STATUS_END['code']

    #执行步骤前进到下一个节点
    def fallback(self):
        WFWaterdrop = get_model('wfwaterdrop')
        wfw = WFWaterdrop.get(WFWaterdrop.c.id == self.__waterdropid)
        if not wfw:
            raise TypeError, 'WFWaterdrop dose not exists.'
        elif wfw.status.code != self.__currentstatus_code:
            raise ValueError, 'WFWaterdrop had passed current status.'
        WFStatusFlow = get_model('wfstatusflow')
        laststatus = wfw.status.statusflow_new.filter(WFStatusFlow.c.waterdrop == self.__waterdropid).order_by(WFStatusFlow.c.created_date.desc()).one()
        if laststatus:
            wfw.status = laststatus.oldstatus
            wfw.save()
            from manager import Waterdrop_manager
            Waterdrop_manager.modify_param(self.__waterdropid, currstatus = laststatus.oldstatus.code, **self.__param_dict)
            if laststatus.oldstatus.autonext:
                self.__currentstatus_code = laststatus.oldstatus.code
                self.__statusflowid = None
                laststatuscode = self.fallback()
            else:
                laststatuscode = laststatus.oldstatus.code
            laststatus.delete()
            return laststatuscode
        return None