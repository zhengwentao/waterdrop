#coding=utf-8
from uliweb.orm import get_model
from uliweb.utils.common import import_attr

from waterdrop import const
from waterdrop.flow import FlowLog

import json


#流程步骤详细执行
class Waterdrop_execute():
    '''
    初始化实例后，必须赋值__info_dict，提供函数名，流程实例ID等必要信息。
    属性__param_dict可选，作为函数执行时的参数输入。
    调用的函数参数定义方式统一为： f(**kwargs)
    '''
    #主要信息：
    #日志ID
    __flowid = None
    #流程实例ID
    __waterdropid = None
    #函数名
    __step = None
    #条件内容
    __condition = None
    #是否是终止步骤
    __is_last = False
    #属性
    __param_dict = None

    #输入属性dict
    def get_param(self):
        return self.__param_dict
    def set_param(self, paramdict = None):
        self.__param_dict = paramdict

    #初始化
    def __init__(self, waterdropid, step, condition, paramdict = None, is_last = False, flowid = None):
        self.__waterdropid = waterdropid
        self.__step = step
        self.__condition = condition
        self.__flowid = flowid
        self.__is_last = is_last
        self.__param_dict = paramdict

    #检查数据库中的flow信息
    def query_flow(self):
        WFFlow = get_model('wfflow')
        #得到记录的流程日志
        if self.__flowid:
            flow = WFFlow.get(WFFlow.c.id == self.__flowid)
        else:
            flow = WFFlow(waterdrop = self.__waterdropid, 
                          step = self.__step,
                          condition = self.__condition,
                          paramsnap = json.dumps(self.__param_dict) if self.__param_dict else '')
            flow.save()
            self.__flowid = flow.id
        return flow

    #写入步骤执行结果
    def save_log_result(self, jresult):
        flow = self.query_flow()
        #记录结果
        flow.result = jresult
        flow.save()

    #写入步骤执行日志状态
    def save_log_status(self, status):
        flow = self.query_flow()
        #记录状态
        flow.status = flow.status + status
        flow.save()

    #将流程设置为终止
    def stop(self):
        WFWaterdrop = get_model('wfwaterdrop')
        wfw = WFWaterdrop.get(WFWaterdrop.c.id == self.__waterdropid)
        if not wfw:
            raise TypeError, 'WFWaterdrop dose not exists.'
        wfw.end = True
        wfw.save()


    #执行步骤
    def execute(self):
        if not self.__step:
            raise KeyError('worlflow step function does not set.')
        #检查函数是否存在
        try:
            _func = import_attr(self.__step)
        except (ImportError, AttributeError) as e:
            FlowLog.error("Can't import function %s" % self.__step)
            raise e
        #写入步骤执行日志--执行中
        self.save_log_status(const.STATUS_EXECUTING)
        try:
            res = _func(workflow_id = self.__waterdropid, **self.__param_dict)
        except Exception, data:
            #写入步骤执行日志--异常
            stat = const.STATUS_ERROR
            res = False
            FlowLog.error(repr(data))
        else:
            #写入步骤执行日志--结束
            stat = const.STATUS_COMPLETED
        self.save_log_status(stat)
        self.save_log_result(res)
        #若此步骤为终止步骤
        if res and self.__is_last:
            self.stop()
        return (stat, res)
