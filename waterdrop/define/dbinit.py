#coding=utf-8
from uliweb import settings
from uliweb.orm import get_model, and_


ERR_BAD_PARAM = -1
#set_dispatch_send(False)
#查找定义数据
initdata = settings.get('WORKFLOWINIT', {})
if len(initdata) < 1:
    exit(ERR_BAD_PARAM)

WFDefine = get_model('wfdefine')
WFStep = get_model('wfstep')
WFCondition = get_model('wfcondition')
WFParam = get_model('wfparam')
WFMix = get_model('wfmix')
WFStatus = get_model('wfstatus')
WFStatusRoute = get_model('wfstatusroute')

#定义赋值
if 'DEFINE' in initdata:
    for each_data in initdata['DEFINE']:
        wfd = WFDefine.get(WFDefine.c.name == each_data['name'])
        if not wfd:
            wfd = WFDefine(name = each_data['name'])
        wfd.desc = each_data['desc']
        wfd.save()
#步骤赋值
if 'STEP' in initdata:
    for each_data in initdata['STEP']:
        wft = WFStep.get(WFStep.c.name == each_data['name'])
        if not wft:
            wft = WFStep(name = each_data['name'])
        wft.function = each_data['function']
        wft.desc = each_data['desc']
        wft.save()
#条件赋值
if 'CONDITION' in initdata:
    for each_data in initdata['CONDITION']:
        wfc = WFCondition.get(WFCondition.c.name == each_data['name'])
        if not wfc:
            wfc = WFCondition(name = each_data['name'])
        wfc.condition = each_data['condition']
        wfc.desc = each_data['desc']
        wfc.save()
#属性赋值
if 'PARAM' in initdata:
    for each_data in initdata['PARAM']:
        wfp = WFParam.get(WFParam.c.name == each_data['name'])
        if not wfp:
            wfp = WFParam(name = each_data['name']) 
        wfp.desc = each_data['desc']
        wfp.valid = each_data['valid']
        if 'default_value' in each_data:
            wfp.default_value = each_data['default_value']
        wfp.save()
#条件与步骤组合MIX
if 'MIX' in initdata:
    for each_data in initdata['MIX']:
        step = WFStep.get(WFStep.c.name == each_data['step'])
        if not step:
            continue
        condition = WFCondition.get(WFCondition.c.name == each_data['condition'])
        if not condition:
            continue
        wfm = WFMix.get(WFMix.c.name == each_data['name'])
        if not wfm:
            wfm = WFMix(name = each_data['name'])
        wfm.desc = each_data['desc']
        wfm.step = step
        wfm.condition = condition
        wfm.save()
#定义与组合m:n关系
if 'DEFINE_MIX' in initdata:
    Define_Mix_Rel = get_model('define_mix_rel')
    for each_data in initdata['DEFINE_MIX']:
        define = WFDefine.get(WFDefine.c.name == each_data['define'])
        if not define:
            continue
        mix = WFMix.get(WFMix.c.name == each_data['mix'])
        if not mix:
            continue
        dmr = Define_Mix_Rel.filter(Define_Mix_Rel.c.define_id == define.id).filter(Define_Mix_Rel.c.mix_id == mix.id).one()
        if not dmr:
            dmr = Define_Mix_Rel(define_id = define, mix_id = mix)
            dmr.save()
if 'STATUS' in initdata:
    for each_data in initdata['STATUS']:
        status = WFStatus.get(WFStatus.c.code == each_data['code'])
        if not status:
            status = WFStatus(code = each_data['code'], name = each_data['name'])
        status.name = each_data['name']
        if 'autonext' in each_data:
            status.autonext = each_data['autonext']
        if 'weight' in each_data:
            status.weight = each_data['weight']
        status.save()
if 'STATUSROUTE' in initdata:
    for each_data in initdata['STATUSROUTE']:
        define = WFDefine.get(WFDefine.c.name == each_data['define'])
        if not define:
            continue
        fs = WFStatus.get(WFStatus.c.code == each_data['from'])
        if not fs:
            continue
        ts = WFStatus.get(WFStatus.c.code == each_data['to'])
        if not ts:
            continue
        wsr = WFStatusRoute.get(and_(WFStatusRoute.c.define == define.id, WFStatusRoute.c.from_status == fs.id, WFStatusRoute.c.to_status == ts.id))
        if not wsr:
            wsr = WFStatusRoute(define = define, from_status = fs, to_status = ts)
        if 'condition' in each_data:
            condition = WFCondition.get(WFCondition.c.name == each_data['condition'])
            if condition:
                wsr.condition = condition
        if 'weight' in each_data:
            wsr.weight = each_data['weight']
        wsr.save()