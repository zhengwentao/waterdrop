#coding=utf-8
from uliweb import settings
from uliweb.orm import get_model


#流程管理类
class Waterdrop_query():

    #通过流程定义ID查询相关步骤和条件
    @staticmethod
    def query_mix_from_define(defineid):
        WFDefine = get_model('wfdefine')
        WFMix = get_model('wfmix')
        WFStep = get_model('wfstep')
        WFCondition = get_model('wfcondition')
        wfd = WFDefine.get(WFDefine.c.id == defineid)
        if not wfd:
            return None
        mixlist = []
        for each_mix in list(wfd.mixs.all()):
            mixlist.append((each_mix.step.to_dict(), each_mix.condition.to_dict(), each_mix.is_last))
        return mixlist

    #通过流程定义ID查询相关步骤和条件
    @staticmethod
    def default_workflow_defineid():
        WFDefine = get_model('wfdefine')
        wfd = WFDefine.filter(WFDefine.c.name == settings.DEFAULT_WORKFLOW.DEFINE).one()
        if not wfd:
            wfd = WFDefine.all().order_by(WFDefine.c.created_date.asc()).one()
        return wfd.id if wfd else -1

    #查询状态
    @staticmethod
    def query_status(code):
        WFStatus = get_model('wfstatus')
        wfs = WFStatus.filter(WFStatus.c.code == code).one()
        return wfs.to_dict() if wfs else None

    #查询下一状态
    @staticmethod
    def query_next_status(code):
        WFStatus = get_model('wfstatus')
        wfs = WFStatus.filter(WFStatus.c.code == code).one()
        if not wfs:
            return None
        WFStatusRoute = get_model('wfstatusroute')
        wfsr = WFStatusRoute.filter(WFStatusRoute.c.from_status == wfs.id)
        return [ {'to_status' : er.to_status.to_dict(), 'condition' : er.condition.to_dict(), 'weight' : er.weight} for er in list(wfsr)]