#coding=utf-8
import logging

#定义handler的输出格式  
formatter = logging.Formatter('%(asctime)s - %(name)s - %(filename)s - %(module)s - %(levelname)s - %(message)s')

#记录流程实例操作日志
WaterdropLog = logging.getLogger('waterdrop')
WaterdropLog.setLevel(logging.DEBUG)
#用于写入日志文件
fh = logging.FileHandler('waterdrop.log')
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
# 给logger添加handler
WaterdropLog.addHandler(fh)
