#coding=utf-8
from uliweb.orm import Field, Model, Reference, ManyToMany, datetime, PICKLE


#定义表，记录流程基本信息
class WFDefine(Model):
    created_date = Field(datetime.datetime, verbose_name='创建时间', auto_now_add=True)
    modified_date = Field(datetime.datetime, verbose_name='修改时间', auto_now=True, auto_now_add=True)
    name = Field(str, max_length = 100, required=True, index=True)
    desc = Field(str, max_length = 500, verbose_name='说明')
    mixs = ManyToMany('wfmix', through='define_mix_rel', collection_name='defines')

#步骤表，记录流程中涉及所有步骤
class WFStep(Model):
    created_date = Field(datetime.datetime, verbose_name='创建时间', auto_now_add=True)
    modified_date = Field(datetime.datetime, verbose_name='修改时间', auto_now=True, auto_now_add=True)
    name = Field(str, max_length = 100, required=True, index=True)
    desc = Field(str, max_length = 500, verbose_name='说明')
    function = Field(str, max_length = 200, verbose_name='步骤函数', index=True)

#步骤与条件的组合，表示在何种条件下执行相应步骤
class WFMix(Model):
    created_date = Field(datetime.datetime, verbose_name='创建时间', auto_now_add=True)
    modified_date = Field(datetime.datetime, verbose_name='修改时间', auto_now=True, auto_now_add=True)
    name = Field(str, max_length = 100, required=True, index=True)
    desc = Field(str, max_length = 500, verbose_name='说明')
    step = Reference('wfstep', collection_name='mix')
    condition = Reference('wfcondition', collection_name='mix')
    is_last = Field(bool, verbose_name='是否最后一个步骤', default = False, required=True, index=True)

#条件表，记录属性组合成的条件内容
class WFCondition(Model):
    created_date = Field(datetime.datetime, verbose_name='创建时间', auto_now_add=True)
    modified_date = Field(datetime.datetime, verbose_name='修改时间', auto_now=True, auto_now_add=True)
    name = Field(str, max_length = 100, required=True, index=True)
    desc = Field(str, max_length = 500, verbose_name='说明')
    condition = Field(str, max_length = 200, verbose_name='条件内容', index=True)

#属性表，记录属性基本信息
class WFParam(Model):
    created_date = Field(datetime.datetime, verbose_name='创建时间', auto_now_add=True)
    modified_date = Field(datetime.datetime, verbose_name='修改时间', auto_now=True, auto_now_add=True)
    name = Field(str, max_length = 100, required=True, index=True)
    desc = Field(str, max_length = 500, verbose_name='说明')
    default_value = Field(PICKLE, verbose_name='默认')
    valid = Field(bool, verbose_name='是否有效', default = False, required=True, index=True)

#组合与流程关联表
class Define_Mix_Rel(Model):
    created_date = Field(datetime.datetime, verbose_name='创建时间', auto_now_add=True)
    modified_date = Field(datetime.datetime, verbose_name='修改时间', auto_now=True, auto_now_add=True)
    mix_id = Reference('wfmix', collection_name='define_mix')
    define_id = Reference('wfdefine', collection_name='define_mix')

#状态信息
class WFStatus(Model):
    code = Field(str, max_length = 50, verbose_name = '状态代码', required = True, index = True, unique = True)
    name  = Field(str, max_length = 200, verbose_name = '状态名称', required = True, index = True)
    valid = Field(bool, verbose_name='有效标志', default = True)
    autonext = Field(bool, verbose_name='自动前进下一状态', default = False)
    weight = Field(float, verbose_name = '权值', default = 1.0)

#状态变更路径信息
class WFStatusRoute(Model):
    define = Reference('wfdefine', collection_name='statusroute')
    from_status = Reference('wfstatus', collection_name='fromstatus')
    to_status = Reference('wfstatus', collection_name='tostatus')
    condition = Reference('wfcondition', collection_name='statusroute')
    weight = Field(float, verbose_name = '权值', default = 1.0)