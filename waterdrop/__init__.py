#coding=utf-8
__version__ = '0.0.4'

from waterdrop.flow.manager import Waterdrop_manager
from mail import MailManager



#流程接口类
class WorkFlowInterface():
    #新增工作流
    @classmethod
    def create_workflow(cls, defineID, **kwargs):
        return Waterdrop_manager.create(defineID, **kwargs)

    #修改属性值
    @classmethod
    def modify_param(cls, workflowid, **kwargs):
        Waterdrop_manager.modify_param(workflowid, **kwargs)

    #发送邮件
    @classmethod
    def send_mail(cls, to_list, subject, content, **kwargs):
        MailManager.push_mail_queue(to_list, subject, content, **kwargs)

    #工作流状态前进
    @classmethod
    def forward_workflow(cls, workflowid, **kwargs):
        return Waterdrop_manager.forward_workflow(workflowid, **kwargs)

    #工作流状态后退
    @classmethod
    def fallback_workflow(cls, workflowid, **kwargs):
        return Waterdrop_manager.fallback_workflow(workflowid, **kwargs)
