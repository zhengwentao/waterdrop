#coding=utf-8
import logging


#定义handler的输出格式  
formatter = logging.Formatter('%(asctime)s - %(name)s - %(filename)s - %(module)s - %(levelname)s - %(message)s')

#记录master日志
MasterLog = logging.getLogger('master')
MasterLog.setLevel(logging.DEBUG)
#用于写入日志文件
fh = logging.FileHandler('master.log')
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
# 给logger添加handler
MasterLog.addHandler(fh)


#记录slave日志
SlaveLog = logging.getLogger('slave')
SlaveLog.setLevel(logging.DEBUG)
#用于写入日志文件
fh = logging.FileHandler('slave.log')
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
# 给logger添加handler
SlaveLog.addHandler(fh)


from uliweb import settings
from uliweb.orm import get_model

#默认的保存日志方案
def default_savelog(logid, slavename, log = None, status = None):
    WFSlaveTask = get_model('wfslavetask')
    wfst = WFSlaveTask.filter(WFSlaveTask.c.logid == logid).one()
    if not wfst:
        wfst = WFSlaveTask(logid = logid, slavename = slavename)
        wfst.save()
    if log:
        wfst.log += log
    if status:
        wfst.status += status
    wfst.save()

#获得开始任务编号
def default_getstartid():
    WFSlaveTask = get_model('wfslavetask')
    wfst = WFSlaveTask.all().order_by(WFSlaveTask.c.logid.desc()).one()
    return wfst.logid+1 if wfst else settings.ASYNC.STARTID