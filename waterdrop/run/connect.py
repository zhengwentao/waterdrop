#coding=utf-8
'''
connect模块

@author: zhengwentao
'''
from uliweb import settings, functions

from waterdrop import const
from . import MasterLog

import types
import functools
import pickle
import json
import gevent


#master类
class Waterdrop_connect(object):

    #slave任务
    __slave_name = None
    __slave_id = None
    __slave_sock = None
    __slave_status = 0
    #任务对象
    __task_obj = None
    __task_type = None
    #异步标志
    __async = True
    #执行进程
    __process = None


    @property
    def slave_id(self):
        return self.__slave_id
    @property
    def status(self):
        return self.__slave_status


    def __init__(self, mtask, mtype, socket, slavename, slaveid):
        self.__task_obj = mtask
        self.__task_type = mtype
        self.__slave_sock = socket
        self.__slave_name = slavename
        self.__slave_id = slaveid

    def __del__(self):
        if self.__slave_sock and not self.__slave_sock.closed:
            self.__slave_sock.close()


    #异步发送任务到slave上
    def send(self, async = True):
        self.__async = async
        self.__process = None
        if self.__async: #异步
            MasterLog.debug('异步任务执行')
            self.__process = gevent.spawn(self._send)
        else: #同步
            MasterLog.debug('同步任务执行')
            self._send()

    #等待任务结束
    def wait(self):
        if not self.__process:
            return
        self.__process.join()

    #中止进程
    def terminate(self):
        if not self.__async or self.__slave_sock.closed:
            return
        try:
            #告知slave中止任务
            self.__slave_sock.sendall(settings.ASYNC.TERMINATE)
            self.__process.join()
        except Exception, e:
            MasterLog.error('中止任务执行报错：' + repr(e))
            self._write_to_log(msg = repr(e), status = const.STATUS_ERROR)

    #发送任务到slave上工作
    def _send(self):
        MasterLog.debug('开始发送任务')
        self._write_to_log(status = const.STATUS_START)
        if not self._format():
            MasterLog.debug('任务格式化失败')
            self._write_to_log(msg = '', status = const.STATUS_ERROR)
            return
        MasterLog.debug('任务格式化成功')
        #发送任务
        try:
            self._write_to_log(status = const.STATUS_EXECUTING)
            #发送json格式内容
            MasterLog.debug('socket发送过程开始')
            self.__slave_sock.sendall(json.dumps(self.__task_obj))
            self.__slave_sock.sendall(settings.ASYNC.END)
            MasterLog.debug('socket发送结束')
            #等待接收结果
            self._recv_intime()
            MasterLog.debug('socket接收结束')
        except Exception, e:
            MasterLog.error('socket执行报错：' + repr(e))
            self._write_to_log(msg = repr(e), status = const.STATUS_ERROR)
        finally:
            MasterLog.debug('socket关闭')
            self.__slave_sock.close()

    #格式化任务内容
    def _format(self):
        try:
            #转换格式，全recv_type部转为DICT
            if self.__task_type == const.TYPE_SCRIPT:
                if isinstance(self.__task_obj, str): #为字符串
                    self.__task_obj = { self.__task_type : self.__task_obj}
            elif self.__task_type == const.TYPE_COMMAND:
                if isinstance(self.__task_obj, str):
                    self.__task_obj = { self.__task_type : self.__task_obj.split(' ') }
                elif isinstance(self.__task_obj, list):
                    self.__task_obj = { self.__task_type : self.__task_obj }
            elif self.__task_type == const.TYPE_FUNCTION:
                if isinstance(self.__task_obj, (types.FunctionType, types.BuiltinFunctionType, functools.partial)):
                    self.__task_obj = { self.__task_type : pickle.dumps(self.__task_obj)}
            else:
                return False
        except Exception, e:
            self._write_to_log(msg = repr(e))
        return True

    #实时接收日志内容
    def _recv_intime(self):
        last_data = ''
        while True:
            data = self.__slave_sock.recv(8192)
            if settings.ASYNC.END in data:
                data = data[:data.find(settings.ASYNC.END)+len(settings.ASYNC.END)]
                #写入（data），结束
                self._write_to_log(msg = data, status = const.STATUS_COMPLETED)
                break
            if settings.ASYNC.ERROR in data:
                data = data[:data.find(settings.ASYNC.ERROR)+len(settings.ASYNC.ERROR)]
                #写入（data），结束
                self._write_to_log(msg = data, status = const.STATUS_ERROR)
                break
            if settings.ASYNC.TERMINATE in data:
                data = data[:data.find(settings.ASYNC.TERMINATE)+len(settings.ASYNC.TERMINATE)]
                #写入（data），结束
                self._write_to_log(msg = data, status = const.STATUS_TERMINATE)
                break
            
            if data:
                #check if end_of_data was split
                last_pair = last_data + data
                if settings.ASYNC.END in last_pair:
                    #结束
                    self._write_to_log(msg = settings.ASYNC.END[len(last_data) - last_pair.find(settings.ASYNC.END):],
                                       status = const.STATUS_COMPLETED)
                    break
                if settings.ASYNC.ERROR in last_pair:
                    #结束
                    self._write_to_log(msg = settings.ASYNC.ERROR[len(last_data) - last_pair.find(settings.ASYNC.ERROR):],
                                       status = const.STATUS_ERROR)
                    break
                if settings.ASYNC.TERMINATE in last_pair:
                    #结束
                    self._write_to_log(msg = settings.ASYNC.TERMINATE[len(last_data) - last_pair.find(settings.ASYNC.TERMINATE):],
                                       status = const.STATUS_TERMINATE)
                    break
                self._write_to_log(msg = data)
                #替换last
                last_data = data
            gevent.sleep(0)

    #msg写入日志
    def _write_to_log(self, msg = None, status = None):
        functions.save_log(self.__slave_id, self.__slave_name, msg, status)
        if status:
            self.__slave_status += status
        if msg:
            MasterLog.info(msg)
