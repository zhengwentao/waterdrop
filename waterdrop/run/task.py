#coding=utf-8
'''
slave模块

@author: zhengwentao
'''

from StringIO import StringIO
from gevent import subprocess
from threading import Thread
from uliweb import settings

from waterdrop import const
from . import SlaveLog

import pickle
import sys
import time
import os
import gevent


#slave-task类
class Waterdrop_task(object):

    #传入任务
    __mtask = None
    #保持的连接
    __sock = None

    def __init__(self, mtask, sock):
        self.__mtask = mtask
        self.__sock = sock


    #发送日志给master
    def _send_log_to_master(self, msg):
        if msg:
            self.__sock.sendall(msg)

    #执行脚本
    def _exec_script(self):
        #写入的文件名称:task_script_时间
        task_script = 'task_script_'+time.strftime('%Y%m%d%H%M%S',time.localtime(time.time()))
        SlaveLog.debug('脚本创建：'+task_script)
        with open(task_script, 'w') as f:
            f.writelines(self.__mtask[const.TYPE_SCRIPT].split('\n'))
        if not os.path.exists(task_script):
            self._send_log_to_master('脚本生成失败！')
            return
        try:
            SlaveLog.debug('脚本开始执行：'+task_script)
            p = subprocess.Popen(['sh', task_script], stdout = subprocess.PIPE, stderr = subprocess.PIPE)
            gevent.spawn(self._read_sub_std(p.poll, p.stdout))
            gevent.spawn(self._read_sub_std(p.poll, p.stderr))
            p.wait()
            #执行结束，返回结果
            self._send_log_to_master(settings.ASYNC.END)
        except Exception, e:
            SlaveLog.error('脚本出错：'+repr(e))
            self._send_log_to_master(repr(e))
            self._send_log_to_master(settings.ASYNC.ERROR)
        finally:
            os.remove(task_script)
            SlaveLog.debug('脚本删除：'+task_script)

    #执行命令行
    def _exec_command(self):
        try:
            SlaveLog.debug('命令行开始执行：'+' '.join(self.__mtask[const.TYPE_COMMAND]))
            p = subprocess.Popen(self.__mtask[const.TYPE_COMMAND], stdout = subprocess.PIPE, stderr = subprocess.PIPE)
            gevent.spawn(self._read_sub_std(p.poll, p.stdout))
            gevent.spawn(self._read_sub_std(p.poll, p.stderr))
            p.wait()
            #执行结束，返回结果
            self._send_log_to_master(settings.ASYNC.END)
        except Exception, e:
            SlaveLog.error('命令行出错：'+repr(e))
            self._send_log_to_master(repr(e))
            self._send_log_to_master(settings.ASYNC.ERROR)

    #subprocess 读取日志
    def _read_sub_std(self, subpoll, substd):
        while subpoll() == None:
            out = substd.readline()
            if out:
                self._send_log_to_master(out)
                SlaveLog.debug('执行结果：'+out)
        else:
            out = substd.read()
            if out:
                self._send_log_to_master(out)
                SlaveLog.debug('执行结果：'+out)

    #执行函数
    def _exec_function(self):
        restd = StringIO()
        self.__console_out = sys.stdout
        self.__console_err = sys.stderr
        sys.stdout = restd
        sys.stderr = restd
        try:
            SlaveLog.debug('函数开始执行：')
            p = Thread(target = pickle.loads(self.__mtask[const.TYPE_FUNCTION]),
                       args = (self.__mtask['param'],) if 'param' in self.__mtask else ())
            p.start()
            while p.isAlive():
                out = restd.getvalue()
                if out:
                    restd.truncate(0)
                    self._send_log_to_master(out)
                    SlaveLog.debug('执行结果：'+out)
                p.join(0)
            else:
                out = restd.getvalue()
                if out:
                    restd.truncate(0)
                    self._send_log_to_master(out)
                    SlaveLog.debug('执行结果：'+out)
            self._send_log_to_master(settings.ASYNC.END)
        except Exception, e:
            SlaveLog.error('函数执行出错：'+repr(e))
            self._send_log_to_master(repr(e))
            self._send_log_to_master(settings.ASYNC.ERROR)
        finally:
            sys.stdout = self.__console_out
            sys.stderr = self.__console_err

    #单任务子进程
    def task(self):
        #脚本
        if const.TYPE_SCRIPT in self.__mtask:
            task_func = self._exec_script
        #命令行
        elif const.TYPE_COMMAND in self.__mtask:
            task_func = self._exec_command
        #函数
        elif const.TYPE_FUNCTION in self.__mtask:
            task_func = self._exec_function
        task_func()
