#coding=utf-8
'''
slave模块

@author: zhengwentao
'''

from gevent.server import StreamServer
from uliweb import settings

from . import SlaveLog
from task import Waterdrop_task

import json
import sys
import gipc
import gevent

#slave类
class Waterdrop_slave(object):

    #socket接收完整信息
    def _recv_all(self, sock):
        total_data = []
        while True:
            try:
                data = sock.recv(8192)
                if settings.ASYNC.END in data:
                    total_data.append(data[:data.find(settings.ASYNC.END)])
                    break
                total_data.append(data)
                if len(total_data)>1:
                    #check if end_of_data was split
                    last_pair=total_data[-2]+total_data[-1]
                    if settings.ASYNC.END in last_pair:
                        total_data[-2]=last_pair[:last_pair.find(settings.ASYNC.END)]
                        total_data.pop()
                        break
            except:
                pass
        return ''.join(total_data)

    #监听master发送的消息
    def _listen_master(self, sock, process):
        last_data = ''
        res = None
        while process.is_alive():
            try:
                data = sock.recv(8192)
                if data:
                    SlaveLog.debug('监视socket：'+data)
                    if settings.ASYNC.TERMINATE in data:
                        res = settings.ASYNC.TERMINATE
                        process.terminate()
                        break
                    last_pair = last_data + data
                    if settings.ASYNC.TERMINATE in last_pair:
                        res = settings.ASYNC.TERMINATE
                        process.terminate()
                        break
                    last_data = data
            except:
                pass
            finally:
                process.join(0)
        return res

    #slave运行主进程
    def run(self):
        reload(sys)
        sys.setdefaultencoding('utf-8')
        #主监听进程
        def handler_send(sock, address):
            result = settings.ASYNC.END 
            try:
                SlaveLog.debug('slave连接开启，开始接收内容')
                master_send = self._recv_all(sock)
                if not master_send:
                    return
                ws = Waterdrop_task(json.loads(master_send, encoding='utf-8'), sock)
                #打开进程执行
                SlaveLog.debug('打开进程开始执行')
                p = gipc.start_process(target = ws.task)
                ls = gevent.spawn(self._listen_master, sock, p)
                ls.join()
                result = ls.get() if ls.get() else result
            except Exception, e:
                result = settings.ASYNC.ERROR
                SlaveLog.error(repr(e))
            finally:
                SlaveLog.debug('slave连接关闭')
                if result != settings.ASYNC.END and not sock.closed:
                    sock.sendall(result)
                    sock.close()
        server = StreamServer(('localhost', settings.ASYNC.SELF_PORT), handler_send)
        server.serve_forever()
