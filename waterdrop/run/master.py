#coding=utf-8
'''
master模块

@author: zhengwentao
'''
from uliweb import settings, functions
from gevent import socket

from . import MasterLog
from connect import Waterdrop_connect

import threading


#master类
class Waterdrop_master(object):
    #单例
    __instance = None  
    #线程安全锁
    __lock = threading.Lock()  
  
    def __init__(self):  
        "disable the __init__ method"  
 
    @classmethod  
    def getInstance(cls):
        if not cls.__instance:  
            cls.__lock.acquire()  
            if not cls.__instance:  
                cls.__instance = object.__new__(cls)  
                object.__init__(cls.__instance)
                #初始化单例
                cls.__instance.init()  
            cls.__lock.release()  
        return cls.__instance

    #slave信息
    __slave_info = {}
    __slave_id = 0
    #定义slave队列
    __queue = {}

    #初始化队列
    def init(self):
        self.__slave_info = settings.ASYNC.SLAVE
        for k in self.__slave_info:
            self.__queue[k] = []
        self.__slave_id = functions.get_startid()

    #派发任务到各个slave
    def send_task(self, mtask, mtype, slavename = None):
        MasterLog.debug('主master开始设置任务')
        self._check_slave_alive()
        if not self._get_slave(slavename):
            raise MemoryError, "Can't find empty slave. Please wait for task completed."
        slavename, address = self._get_slave(slavename)
        slave_sock = socket.socket()
        MasterLog.debug('主master：socket开始连接')
        slave_sock.connect(address)
        MasterLog.debug('主master：socket连接成功')
        slaveid = self.__slave_id
        self.__slave_id += 1
        wc = Waterdrop_connect(mtask, mtype, slave_sock, slavename, slaveid)
        wc.send()
        self.__queue[slavename].append(wc)
        return slaveid

    #中止指定slave上所有任务
    def terminate_slave(self, slavename):
        MasterLog.debug('主master中止特定slave')
        if not self._check_slave_exists(slavename):
            raise NameError, "Can't find slave by name."
        map(lambda gwc: gwc.terminate(), self.__queue[slavename])
        self.__queue[slavename][:] = []
        MasterLog.debug('主master中止特定slave成功')

    #中止指定任务
    def terminate_task(self, task_id):
        MasterLog.debug('主master中止特定任务')
        for each_slave in self.__queue.values():
            kill_gwc = None
            for gwc in each_slave:
                if gwc.slave_id == task_id:
                    gwc.terminate()
                    kill_gwc = gwc
                    break
            if kill_gwc:
                each_slave.remove(kill_gwc)
                MasterLog.debug('主master中止特定任务成功')
                break


    #获得当前任务最少的slave
    def _get_slave(self, slavename = None):
        #指定slave
        if self._check_slave_exists(slavename):
            return (slavename, self.__slave_info[slavename]['address']) if not self._check_slave_full(slavename) else None
        #自动查找空闲slave
        else:
            for k in self.__slave_info:
                if not self._check_slave_full(k):
                    return (k, self.__slave_info[k]['address']) 
            return None

    #检查slavename对应的slave是否存在：存在-True，不存在-False
    def _check_slave_exists(self, slavename):
        return slavename and slavename in self.__slave_info

    #检查slave队列是否已满：满-True，不满-False
    def _check_slave_full(self, slavename):
        return len(self.__queue[slavename]) >= self.__slave_info[slavename]['maxcount']

    #检查各slave队列socket工作状态
    def _check_slave_alive(self):
        for each_slave in self.__queue.values():
            for i in range(len(each_slave)):
                gwc = each_slave.pop()
                if len(bin(gwc.status)) <= 4:
                    each_slave.insert(0, gwc)
