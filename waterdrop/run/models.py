#coding=utf-8
from uliweb.orm import Field, Model, BLOB, datetime


#slave任务日志表，记录slave任务日志状态
class WFSlaveTask(Model):
    created_date = Field(datetime.datetime, verbose_name='创建时间', auto_now_add=True)
    modified_date = Field(datetime.datetime, verbose_name='修改时间', auto_now=True, auto_now_add=True)
    slavename = Field(str, max_length = 50, verbose_name='slave名称', required=True)
    log = Field(BLOB, verbose_name='slave日志', default = '')
    status = Field(int, verbose_name='执行状态', default = 0, required=True, index=True)
    logid = Field(int, verbose_name='日志编号', default = 0, required=True, index=True)
