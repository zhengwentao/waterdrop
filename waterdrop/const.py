#coding=utf-8
'''
常量类

@author: zhengwentao
'''
import sys 

#定义常量类
class _const():
    class ConstError(TypeError):pass 
    def __setattr__(self, name, value): 
        if self.__dict__.has_key(name): 
            raise self.ConstError, "Can't rebind const (%s)" %name 
        self.__dict__[name]=value 
    #任务运行状态
    STATUS_START     = 1
    STATUS_EXECUTING = 2
    STATUS_COMPLETED = 4
    STATUS_ERROR     = 8
    STATUS_TERMINATE = 16
    #发送任务类型
    TYPE_SCRIPT   = 'script'
    TYPE_COMMAND  = 'command'
    TYPE_FUNCTION = 'function'

#可供导入模块
sys.modules[__name__] = _const()