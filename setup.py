import waterdrop
from uliweb.utils.setup import setup

__doc__ = """
Waterdrop
=========

About Waterdrop
----------------
Uliweb is a Python based web framework created by limodou <limodou@gmail.com>, 
and Waterdrop is an apps collection project for uliweb framework to provide a simple workflow engine.

License
------------
Waterdrop is released under BSD license.
"""

setup(name = 'waterdrop',
    version = waterdrop.__version__,
    author = "Zheng Wen Tao",
    author_email = "zwtzjd@gmail.com",
    description = "Easy python workflow framework",
    long_description = __doc__,
    classifiers = [
    "Development Status :: 3 - Alpha",
    "Environment :: Web Environment",
    "Intended Audience :: Developers",
    "License :: OSI Approved :: BSD License",
    "Topic :: Internet :: WWW/HTTP :: WSGI",
    "Programming Language :: Python",
    "Operating System :: OS Independent",
    "Programming Language :: Python :: 2.6",
    "Programming Language :: Python :: 2.7",
    ],
    license = "BSD",
    packages = ['waterdrop'],
    platforms = 'any',
    include_package_data = True,
    zip_safe = False,
    entry_points = {
        'uliweb_apps': [
          'helpers = waterdrop',
        ],
    },
)
