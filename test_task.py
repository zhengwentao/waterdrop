#coding=utf-8
from uliweb import settings
from uliweb.utils.common import import_attr
from gevent.queue import Queue
from gevent.event import AsyncResult

import gevent
import gipc



class TaskManager():

    #启动管理进程及其配置的协程，监控任务
    def activate(self):
        #构造任务进程输入组
        exectaskqueue = Queue()
        taskglist = [gevent.spawn(self._listen, taskdict = etaskstr, exectaskqueue = exectaskqueue) for etaskstr in settings.DAEMON.children]
        #事件监听
        signalevent = AsyncResult()
        gsignal = gevent.spawn(self.getsignal, signalevent = signalevent)
        #中止task监听
        gkill = gevent.spawn(self.kill, getevent = signalevent, exectaskqueue = exectaskqueue)
        gevent.joinall(taskglist + [gsignal, gkill])

    #监听协程
    def _listen(self, taskdict, exectaskqueue):
        #构造检查进程
        handleself, handleparam = gipc.pipe(duplex = True)
        gipc.start_process(target = self._gettaskparam,
                                   kwargs = {'paramfun' : import_attr(taskdict['param']), 'pipehandle' : handleparam},
                                   name = taskdict['name'])
        #构造任务进程输入组
        taskqueue = Queue()
        for i in xrange(taskdict['process_num']):
            handletask, handleexec = gipc.pipe(duplex = True)
            kwargs = {'execfun' : import_attr(taskdict['exec']), 'pipehandle' : handleexec}
            execprocess = gipc.start_process(target = self._exectask, kwargs = kwargs, name = taskdict['name']+str(i))
            exectaskqueue.put((execprocess, kwargs))
            taskqueue.put(handletask)
        #开始输入参数
        while True:
            execparam = handleself.get()
            brock_num = 0
            while True:
                handletask = taskqueue.get()
                try:
                    handletask.get(timeout = gevent.Timeout(0.1))
                    handletask.put(execparam)
                    break
                except EOFError:
                    brock_num += 1
                finally:
                    taskqueue.put(handletask)
                if brock_num == taskdict['process_num']:
                    gevent.sleep(0)
            handleself.put('continue')

    #检查任务参数是否存在
    def _gettaskparam(self, paramfun, pipehandle):
        while True:
            cres = paramfun()
            if cres:
                pipehandle.put(cres)
                pipehandle.get()
            else:
                gevent.sleep(settings.DAEMON.param_timeout)

    #执行任务
    def _exectask(self, execfun, pipehandle):
        pipehandle.put('start')
        while True:
            fparam = pipehandle.get()
            execfun(fparam)
            pipehandle.put('finished')

    #中止任务
    def kill(self, getevent, exectaskqueue):
        while True:
            eventdict = getevent.get()
            if 'kill' in eventdict:
                for i in xrange(exectaskqueue.qsize()):
                    execprocess, kwargs = exectaskqueue.get()
                    if eventdict['kill'] == execprocess.name:
                        execprocess.terminate()
                        execprocess = gipc.start_process(target = self._exectask, kwargs = kwargs, name = eventdict['kill'])
                        exectaskqueue.put((execprocess, kwargs))
                        break
                    else:
                        exectaskqueue.put((execprocess, kwargs))

    #获得外部信号
    def getsignal(self, signalevent):
        while True:
            signaldict = import_attr(settings.DAEMON.signal)()
            if signaldict:
                signalevent.set(signaldict)
            gevent.sleep(settings.DAEMON.param_timeout)
