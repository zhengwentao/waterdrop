#coding=utf-8
from uliweb import functions
from uliweb.core.commands import Command
from optparse import make_option
from gevent.server import StreamServer

import gevent


def test_function():
    print 'xxxxxx'
    gevent.sleep(6)
    print '121212121'


class MasterCommand(Command):
    name = 'mastertest'
    option_list = (
        #启动master分配的任务
        make_option('-s', '--send', action = 'store', type = 'string', default = '', dest = 'sendtask', help = 'Send task from master to slave'),
        #启动master分配的任务类型
        make_option('-t', '--type', action = 'store', type = 'string', default = '', dest = 'sendtype', help = 'Send task type'),
        #test-function
        make_option('-f', '--function', action = 'store_true', default = False, dest = 'functiontest', help = 'Send task function'),
        #test-master
        make_option('-m', '--master', action = 'store_true', default = False, dest = 'master_send', help = 'Master send task'),
    )
    check_apps_dirs = True
    has_options = True
    check_apps = False

    def handle(self, options, global_options, *args):
        from waterdrop.run.master import Waterdrop_master
        self.get_application(global_options)
        wm = Waterdrop_master.getInstance()
        if options.sendtask:
            tid = wm.send_task(options.sendtask, options.sendtype)
            gevent.sleep(20)
            wm.terminate_task(tid)
            print 'out'
        elif options.functiontest:
            print 'out1'
            wm.send_task(test_function, 'function')
            print 'out2'
        elif options.master_send:
            wm.send_task(test_function, 'function')
            wm.send_task(test_function, 'function')
        else:
            self.print_help("uliweb", self.name)
        gevent.sleep(10)


class SlaveCommand(Command):
    name = 'slavetest'
    option_list = (
        #启动master分配的任务
        make_option('-r', '--run', action = 'store_true', default = False, dest = 'runtask', help = 'Run task from master.'),
    )
    help = ''
    args = ''
    check_apps_dirs = True
    has_options = True
    check_apps = False

    def handle(self, options, global_options, *args):
        from waterdrop.run.slave import Waterdrop_slave
        self.get_application(global_options)
        if options.runtask:
            ws = Waterdrop_slave()
            ws.run()
